use std::str::FromStr;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum FileTypeError {
    #[error("Unknown FileType: `{0}`")]
    UnknownError(String),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum FileType {
    Unknown,
    Json,
    Yaml,
    Ron,
    Pdf,
    Html,
}

impl FromStr for FileType {
    type Err = FileTypeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.to_lowercase();
        match s.as_str() {
            "json" => Ok(Self::Json),
            "yml" | "yaml" => Ok(Self::Yaml),
            "ron" => Ok(Self::Ron),
            "pdf" => Ok(Self::Pdf),
            "html" => Ok(Self::Html),
            _ => Err(FileTypeError::UnknownError(s)),
        }
    }
}

pub fn from_filename(filename: &str) -> FileType {
    let filename = filename.to_lowercase();
    let len = filename.len();
    let last_three = &filename[(len - 3)..len];
    match last_three {
        "pdf" => return FileType::Pdf,
        "ron" => return FileType::Ron,
        "yml" => return FileType::Yaml,
        _ => {}
    }
    let last_four = &filename[(len - 4)..len];
    match last_four {
        "yaml" => return FileType::Yaml,
        "json" => return FileType::Json,
        "html" => return FileType::Html,
        _ => {}
    }
    FileType::Unknown
}

pub fn from_string(data: &str) -> FileType {
    if serde_json::from_str::<serde_json::Value>(data).is_ok() {
        return FileType::Json;
    }
    if serde_yaml::from_str::<serde_yaml::Value>(data).is_ok() {
        return FileType::Yaml;
    }
    if ron::from_str::<ron::Value>(data).is_ok() {
        return FileType::Ron;
    }
    FileType::Unknown
}
