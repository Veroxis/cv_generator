use std::path::PathBuf;

use clap::Parser;
use cv_generator::cv::Cv;
use cv_generator::cv::CvError;
use thiserror::Error;

use crate::filetype;
use crate::filetype::FileType;

#[derive(Debug, Error)]
pub enum ConvertError {
    #[error("CantReadFileToString: {0}")]
    CantReadFileToString(std::io::Error),

    #[error("CantExportToType: {0}")]
    CvError(#[from] CvError),

    #[error("CantExportToType: {0}")]
    CantExportToType(String),

    #[error("CantReadCurrentDir: {0}")]
    CantReadCurrentDir(std::io::Error),

    #[error("CantSetCurrentDir: {0}")]
    CantSetCurrentDir(std::io::Error),

    #[error("ParentDirInvalid: {0}")]
    ParentDirInvalid(std::path::PathBuf),

    #[error("CantReadFilename: {0}")]
    CantReadFilename(std::path::PathBuf),
}

#[derive(Debug, Parser)]
#[clap()]
pub struct ConvertArgs {
    /// Set the output filename. It's file ending is used to select the exported file format: example.yaml, example.json, example.ron, example.html or example.pdf
    #[clap(short, long)]
    pub output: Vec<PathBuf>,

    /// Set the source filename. Allowed input formats are yaml, json or ron
    #[clap()]
    pub filename: PathBuf,
}

pub fn run(args: ConvertArgs) -> Result<(), ConvertError> {
    let raw_data =
        std::fs::read_to_string(&args.filename).map_err(ConvertError::CantReadFileToString)?;
    let cv = Cv::try_from(raw_data.as_str())?;

    let basedir = std::env::current_dir().map_err(ConvertError::CantReadCurrentDir)?;
    let converter_workdir = &args
        .filename
        .parent()
        .ok_or_else(|| ConvertError::ParentDirInvalid(args.filename.clone()))?;
    std::env::set_current_dir(converter_workdir).map_err(ConvertError::CantSetCurrentDir)?;

    for output_path in &args.output {
        tracing::info!("creating output: `{:?}`", output_path);

        let savepath = PathBuf::from_iter(&[&basedir, output_path])
            .to_string_lossy()
            .to_string();

        let output_filetype = filetype::from_filename(&savepath);
        tracing::info!("detected output filetype: `{:?}`", output_filetype);
        if output_filetype == FileType::Unknown {
            return Err(ConvertError::CantExportToType("Unknown".to_owned()));
        }
        scopeguard::defer!({
            tracing::info!("generated file \"{}\"", savepath.as_str());
        });
        match output_filetype {
            FileType::Unknown => unreachable!(),
            FileType::Json => cv.save_as_json(savepath.as_str())?,
            FileType::Yaml => cv.save_as_yaml(savepath.as_str())?,
            FileType::Ron => cv.save_as_ron(savepath.as_str())?,
            FileType::Pdf => cv.save_as_pdf(savepath.as_str())?,
            FileType::Html => cv.save_as_html(savepath.as_str())?,
        };
    }
    Ok(())
}
