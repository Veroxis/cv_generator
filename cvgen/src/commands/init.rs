use std::path::PathBuf;

use clap::Parser;
use cv_generator::cv::Cv;
use cv_generator::cv::CvError;
use thiserror::Error;

use crate::filetype::FileType;
use crate::filetype::{self};

#[derive(Debug, Error)]
pub enum InitError {
    #[error("CantExportToType: {0}")]
    CantExportToType(String),

    #[error("CantExportToType: {0}")]
    CvError(#[from] CvError),
}

#[derive(Debug, Parser)]
#[clap()]
pub struct InitArgs {
    /// Set the output filename. It's file ending is used to select the exported file format: example.yaml, example.json, example.ron, example.html or example.pdf
    #[clap()]
    pub output: PathBuf,
}

pub fn run(args: InitArgs) -> Result<(), InitError> {
    let savepath = args.output.to_string_lossy().to_string();
    let filetype = filetype::from_filename(savepath.as_str());
    tracing::info!("parsed filetype from filename: `{:?}`", filetype);
    let cv = Cv::create_example();
    match filetype {
        FileType::Unknown => Err(InitError::CantExportToType("Unknown".to_owned())),
        FileType::Json => Ok(cv.save_as_json(savepath.as_str())?),
        FileType::Yaml => Ok(cv.save_as_yaml(savepath.as_str())?),
        FileType::Ron => Ok(cv.save_as_ron(savepath.as_str())?),
        FileType::Pdf => Ok(cv.save_as_pdf(savepath.as_str())?),
        FileType::Html => Ok(cv.save_as_html(savepath.as_str())?),
    }
}
