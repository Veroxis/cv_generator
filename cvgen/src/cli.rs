use clap::Parser;

use crate::commands::convert::ConvertArgs;
use crate::commands::init::InitArgs;

#[derive(Debug, Parser)]
#[clap(version)]
pub struct AppArgs {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Parser)]
#[clap()]
pub enum Commands {
    /// Create an example configuration file
    Init(InitArgs),
    /// Convert your config file to one of the allowed output formats
    Convert(ConvertArgs),
}
