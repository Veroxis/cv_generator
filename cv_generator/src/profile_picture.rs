use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Serialize, Deserialize)]
pub struct ProfilePicture {
    pub height: Option<u32>,
    pub path: String,
    pub width: Option<u32>,
}

impl Default for ProfilePicture {
    fn default() -> Self {
        Self {
            path: "".to_string(),
            width: None,
            height: None,
        }
    }
}

impl ProfilePicture {
    pub fn new(path: String, width: Option<u32>, height: Option<u32>) -> Self {
        Self {
            path,
            width,
            height,
        }
    }

    pub fn to_html(&self) -> String {
        let mut attributes = format!("src=\"{path}\"", path = self.path);
        if let Some(width) = self.width {
            attributes = format!("{attributes} width=\"{width}\"");
        }
        if let Some(height) = self.height {
            attributes = format!("{attributes} height=\"{height}\"");
        }
        format!("<img {attributes} />")
    }
}
