use serde::Deserialize;
use serde::Serialize;

use crate::section::Section;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Chapter {
    title: String,
    sections: Vec<Section>,
}

impl Chapter {
    pub fn from(title: String, sections: Vec<Section>) -> Self {
        Chapter { title, sections }
    }

    pub fn to_html(&self) -> String {
        let mut content = String::new();
        for section in &self.sections {
            content.push_str(&section.to_html());
        }
        format!(
            "<chapter><h2>{title}</h2>{elements}</chapter>",
            title = self.title,
            elements = content
        )
    }
}
