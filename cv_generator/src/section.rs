use serde::Deserialize;
use serde::Serialize;

use crate::content_element::ContentElement;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Section {
    title: String,
    content_elements: Vec<ContentElement>,
}

impl Section {
    pub fn from(title: String, content_elements: Vec<ContentElement>) -> Self {
        Self {
            title,
            content_elements,
        }
    }

    pub fn to_html(&self) -> String {
        let mut content = String::new();
        for element in &self.content_elements {
            content.push_str(&element.to_html());
        }
        format!(
            "<section><left><h3>{title}</h3></left><right>{elements}</right></section>",
            title = self.title,
            elements = content
        )
    }
}
