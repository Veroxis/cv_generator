use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type", content = "content")]
pub enum ContentElement {
    List(Vec<ContentElement>),
    OrderedList(Vec<ContentElement>),
    Paragraph(String),
}

impl ContentElement {
    pub fn to_html(&self) -> String {
        match self {
            ContentElement::Paragraph(content) => format!("<p>{content}</p>"),
            ContentElement::List(entries) => {
                let mut list_content = String::new();
                for entry in entries {
                    list_content = format!("{list_content}<li>{}</li>", entry.to_html());
                }
                format!("<ul>{}</ul>", &list_content)
            }
            ContentElement::OrderedList(entries) => {
                let mut list_content = String::new();
                for entry in entries {
                    list_content = format!("{list_content}<li>{}</li>", entry.to_html());
                }
                format!("<ol>{}</ol>", &list_content)
            }
        }
    }
}
