use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use std::process::Stdio;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum PdfError {
    #[error("ChromiumNotFound")]
    ChromiumNotFound,
    #[error("CantCreateTempFile")]
    CantCreateTempFile,
    #[error("CantDeleteTempFile")]
    CantDeleteTempFile,
    #[error("CantWriteToTempFile")]
    CantWriteToTempFile,
    #[error("CantExecuteChromium")]
    CantExecuteChromium,
    #[error("ChromiumFailedWithError")]
    ChromiumFailedWithError,
}

pub fn html_to_pdf(html: &str, pdf_save_path: &Path) -> Result<(), PdfError> {
    let pdf_tmp_path = create_tmp_html_file(html)?;
    scopeguard::defer!({
        let _ = delete_tmp_html_file(pdf_tmp_path.as_path());
    });
    htmlfile_to_pdf(pdf_tmp_path.as_path(), pdf_save_path)?;

    Ok(())
}

pub fn htmlfile_to_pdf(html_path: &Path, pdf_save_path: &Path) -> Result<(), PdfError> {
    let pdf_save_path = pdf_save_path.to_string_lossy().to_string();
    let html_path = html_path.to_string_lossy().to_string();
    let browser_binary = find_chromium_binary()?;
    tracing::debug!("generating pdf using `{browser_binary}`...");
    let cmd = Command::new(browser_binary)
        .args([
            "--headless",
            "--disable-gpu",
            "--print-to-pdf-no-header",
            "--run-all-compositor-stages-before-draw",
            &format!("--print-to-pdf={pdf_save_path}"),
            &html_path,
        ])
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .output()
        .map_err(|_| PdfError::CantExecuteChromium)?;
    if !cmd.status.success() {
        return Err(PdfError::ChromiumFailedWithError);
    }
    tracing::debug!("pdf was created: `{pdf_save_path}`");
    Ok(())
}

fn find_chromium_binary() -> Result<&'static str, PdfError> {
    tracing::debug!("searching for known chromium executables...");
    let mut result = None;
    let chromium_known_binary_names = [
        "chromium",
        "chromium-bin",
        "google-chrome",
        "google-chrome-bin",
    ];
    for name in chromium_known_binary_names.iter() {
        match which::which(name) {
            Ok(_) => {
                result = Some(*name);
                tracing::debug!("found `{name}`");
                break;
            }
            Err(_) => tracing::debug!("executable `{name}` doesn't exist"),
        }
    }
    match result {
        Some(result) => Ok(result),
        None => Err(PdfError::ChromiumNotFound),
    }
}

fn create_tmp_html_file(html: &str) -> Result<PathBuf, PdfError> {
    let pdf_tmp_path = format!("/tmp/cv_generator_tmp_file-{}.html", uuid::Uuid::new_v4());
    File::create(pdf_tmp_path.as_str())
        .map_err(|_| PdfError::CantCreateTempFile)?
        .write_all(html.as_bytes())
        .map_err(|_| PdfError::CantWriteToTempFile)?;
    Ok(PathBuf::from(pdf_tmp_path))
}

fn delete_tmp_html_file(path: &Path) -> Result<(), PdfError> {
    fs::remove_file(path).map_err(|_| PdfError::CantDeleteTempFile)?;
    Ok(())
}
