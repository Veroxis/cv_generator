use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::PathBuf;

use grass::OutputStyle;
use ron::error::SpannedError;
use serde::Deserialize;
use serde::Serialize;
use thiserror::Error;

use crate::chapter::Chapter;
use crate::content_element::ContentElement;
use crate::cv_element::CvElement;
use crate::pdf::html_to_pdf;
use crate::pdf::PdfError;
use crate::profile_picture::ProfilePicture;
use crate::section::Section;

#[derive(Debug, Error)]
pub enum CvError {
    #[error("SerdeJsonError: `{0}`")]
    SerdeJsonError(#[from] serde_json::Error),
    #[error("SerdeYamlError: `{0}`")]
    SerdeYamlError(#[from] serde_yaml::Error),
    #[error("RonError: `{0}`")]
    RonError(#[from] ron::Error),
    #[error("PdfError: `{0}`")]
    PdfError(#[from] PdfError),
    #[error("GrassError: `{0}`")]
    GrassError(#[from] Box<grass::Error>),
    #[error("SpannedError: `{0}`")]
    SpannedError(#[from] SpannedError),
    #[error("CantReadFile: `{0}`")]
    CantReadFile(std::io::Error),
    #[error("CantWriteFile: `{0}`")]
    CantWriteFile(std::io::Error),
    #[error("CantCreateFile: `{0}`")]
    CantCreateFile(std::io::Error),
    #[error("CantOpenFile: `{0}`")]
    CantOpenFile(std::io::Error),
    #[error("CantReadWorkdir: `{0}`")]
    CantReadWorkdir(std::io::Error),
    #[error("CantDeserialize")]
    CantDeserialize,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cv {
    title: String,
    profile_picture: Option<ProfilePicture>,
    accent_color: String,
    custom_scss_path: Option<String>,
    elements: Vec<CvElement>,
}

impl Default for Cv {
    fn default() -> Self {
        Cv {
            title: String::from("Curriculum Vitae"),
            profile_picture: None,
            accent_color: String::from("#1793d0"),
            custom_scss_path: None,
            elements: vec![],
        }
    }
}

impl TryFrom<&str> for Cv {
    type Error = CvError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        tracing::info!("trying to parse cv input...");

        match Cv::from_json(value) {
            Ok(cv) => {
                tracing::info!("json: success");
                return Ok(cv);
            }
            Err(error) => tracing::info!("json: error: {error}"),
        };

        match Cv::from_yaml(value) {
            Ok(cv) => {
                tracing::info!("yaml: success");
                return Ok(cv);
            }
            Err(error) => tracing::info!("yaml: error: {error}"),
        };

        match Cv::from_ron(value) {
            Ok(cv) => {
                tracing::info!("ron: success");
                return Ok(cv);
            }
            Err(error) => tracing::info!("ron: error: {error}"),
        };

        Err(CvError::CantDeserialize)
    }
}

impl Cv {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn create_example() -> Self {
        let mut cv = Cv {
            profile_picture: Some(ProfilePicture {
                    height: None,
                    path: "https://www.pngfind.com/pngs/m/610-6104451_image-placeholder-png-user-profile-placeholder-image-png.png".to_string(),
                    width: Some(200)
                }),
                ..Default::default()
        };
        let chapter_dummy_a = CvElement::Chapter(Chapter::from(
            "Lorem Ipsum".to_string(),
            vec![
                Section::from(
                    "dolor sit amet".to_string(),
                    vec![
                        ContentElement::Paragraph("consectetur adipiscing elit".to_string()),
                        ContentElement::OrderedList(vec![
                            ContentElement::Paragraph("Ut enim ad".to_string()),
                            ContentElement::Paragraph("minim veniam".to_string()),
                        ]),
                        ContentElement::Paragraph(
                            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
                                .to_string(),
                        ),
                        ContentElement::List(vec![
                            ContentElement::Paragraph(
                                "quis nostrud exercitation ullamco".to_string(),
                            ),
                            ContentElement::Paragraph("sunt in culpa qui officia".to_string()),
                        ]),
                    ],
                ),
                Section::from(
                    "consectetur adipiscing elit".to_string(),
                    vec![
                        ContentElement::Paragraph("consectetur adipiscing elit".to_string()),
                        ContentElement::OrderedList(vec![
                            ContentElement::Paragraph("Ut enim ad".to_string()),
                            ContentElement::Paragraph("minim veniam".to_string()),
                        ]),
                        ContentElement::Paragraph(
                            "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
                                .to_string(),
                        ),
                        ContentElement::List(vec![
                            ContentElement::Paragraph(
                                "quis nostrud exercitation ullamco".to_string(),
                            ),
                            ContentElement::Paragraph("sunt in culpa qui officia".to_string()),
                        ]),
                    ],
                ),
            ],
        ));
        let chapter_dummy_b = CvElement::Chapter(Chapter::from(
            "Lorem Ipsum".to_string(),
            vec![
                Section::from(
                    "dolor sit amet".to_string(),
                    vec![
                        ContentElement::List(vec![
                            ContentElement::Paragraph("quis nostrud exercitation".to_string()),
                            ContentElement::Paragraph("sunt in culpa qui officia".to_string()),
                        ]),
                        ContentElement::Paragraph("consectetur adipiscing elit".to_string()),
                        ContentElement::Paragraph("sed do eiusmod tempor incididunt".to_string()),
                        ContentElement::OrderedList(vec![
                            ContentElement::Paragraph("Ut enim ad".to_string()),
                            ContentElement::Paragraph("minim veniam".to_string()),
                        ]),
                    ],
                ),
                Section::from(
                    "consectetur adipiscing elit".to_string(),
                    vec![
                        ContentElement::Paragraph("consectetur adipiscing elit".to_string()),
                        ContentElement::Paragraph("sed do et dolore magna aliqua".to_string()),
                        ContentElement::List(vec![
                            ContentElement::Paragraph("quis nostrud ullamco".to_string()),
                            ContentElement::Paragraph("sunt in culpa qui officia".to_string()),
                        ]),
                    ],
                ),
            ],
        ));
        cv.add_elements(vec![
            chapter_dummy_b.clone(),
            chapter_dummy_a.clone(),
            chapter_dummy_b.clone(),
            CvElement::Break,
            chapter_dummy_b,
            chapter_dummy_a,
        ]);
        cv
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title;
    }

    pub fn set_profile_picture(&mut self, profile_picture: Option<ProfilePicture>) {
        self.profile_picture = profile_picture;
    }

    pub fn set_accent_color(&mut self, color: String) {
        self.accent_color = color;
    }

    pub fn add_element(&mut self, element: CvElement) {
        self.elements.push(element);
    }

    pub fn add_elements(&mut self, element: Vec<CvElement>) {
        self.elements.extend(element);
    }

    fn load_style(&self) -> Result<String, CvError> {
        let mut scss = String::from(include_str!("static_style.scss"));
        scss = format!("$accent_color: {};{}", &self.accent_color, scss);
        let options = grass::Options::default().style(OutputStyle::Compressed);
        Ok(grass::from_string(scss, &options)?)
    }

    pub fn to_html(&self) -> Result<String, CvError> {
        let mut cv_content = format!("<h1>{}</h1>", &self.title);
        for element in &self.elements {
            cv_content.push_str(&element.to_html());
        }
        let css = self.load_style()?;
        let workdir = std::env::current_dir()
            .map_err(CvError::CantReadWorkdir)?
            .to_string_lossy()
            .to_string();
        if let Some(profile_picture) = &self.profile_picture {
            cv_content = format!("{}{}", profile_picture.to_html(), &cv_content);
        }
        let document = format!(
            r#"<!DOCTYPE html><html lang="en"><head><base href="{base_path}/"><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>{title}</title><style>@import url('https://fonts.googleapis.com/css2?family=Ubuntu&display=swap');</style><style>{css}</style></head><body>{cv}</body></html>{new_line}"#,
            title = self.title,
            base_path = workdir,
            css = &css,
            cv = &cv_content,
            new_line = "\n"
        );
        Ok(document)
    }

    pub fn add_custom_scss_path(&mut self, scss_path: Option<String>) {
        self.custom_scss_path = scss_path;
    }

    pub fn to_yaml(&self) -> Result<String, CvError> {
        Ok(serde_yaml::to_string(&self)?)
    }

    pub fn from_yaml(yaml: &str) -> Result<Self, CvError> {
        Ok(serde_yaml::from_str::<Cv>(yaml)?)
    }

    pub fn to_json(&self) -> Result<String, CvError> {
        Ok(serde_json::to_string(&self)?)
    }

    pub fn from_json(json: &str) -> Result<Self, CvError> {
        Ok(serde_json::from_str::<Cv>(json)?)
    }

    pub fn to_ron(&self) -> Result<String, CvError> {
        Ok(ron::to_string(&self)?)
    }

    pub fn from_ron(ron: &str) -> Result<Self, CvError> {
        Ok(ron::from_str(ron)?)
    }

    pub fn load_from_yaml(path: &str) -> Result<Self, CvError> {
        let mut file = File::open(path).map_err(CvError::CantOpenFile)?;
        let mut yaml = String::new();
        file.read_to_string(&mut yaml)
            .map_err(CvError::CantReadFile)?;
        Ok(serde_yaml::from_str::<Cv>(&yaml)?)
    }

    pub fn load_from_json(path: &str) -> Result<Self, CvError> {
        let mut file = File::open(path).map_err(CvError::CantOpenFile)?;
        let mut json = String::new();
        file.read_to_string(&mut json)
            .map_err(CvError::CantReadFile)?;
        Ok(serde_json::from_str::<Cv>(&json)?)
    }

    pub fn save_as_yaml(&self, path: &str) -> Result<(), CvError> {
        tracing::debug!("save_as_yaml: `{path}`");
        let yaml = self.to_yaml()?;
        let mut curriculum_vitae_html_file =
            std::fs::File::create(path).map_err(CvError::CantCreateFile)?;
        curriculum_vitae_html_file
            .write_all(yaml.as_bytes())
            .map_err(CvError::CantWriteFile)?;
        Ok(())
    }

    pub fn save_as_json(&self, path: &str) -> Result<(), CvError> {
        tracing::debug!("save_as_json: `{path}`");
        let json = self.to_json()?;
        let mut curriculum_vitae_html_file =
            std::fs::File::create(path).map_err(CvError::CantCreateFile)?;
        curriculum_vitae_html_file
            .write_all(json.as_bytes())
            .map_err(CvError::CantWriteFile)?;
        Ok(())
    }

    pub fn save_as_ron(&self, path: &str) -> Result<(), CvError> {
        tracing::debug!("save_as_ron: `{path}`");
        let ron = self.to_ron()?;
        let mut curriculum_vitae_html_file =
            std::fs::File::create(path).map_err(CvError::CantCreateFile)?;
        curriculum_vitae_html_file
            .write_all(ron.as_bytes())
            .map_err(CvError::CantWriteFile)?;
        Ok(())
    }

    pub fn save_as_html(&self, path: &str) -> Result<(), CvError> {
        tracing::debug!("save_as_html: `{path}`");
        let html = self.to_html()?;
        let mut curriculum_vitae_html_file =
            std::fs::File::create(path).map_err(CvError::CantCreateFile)?;
        curriculum_vitae_html_file
            .write_all(html.as_bytes())
            .map_err(CvError::CantWriteFile)?;
        Ok(())
    }

    pub fn save_as_pdf(&self, path: &str) -> Result<(), CvError> {
        tracing::debug!("save_as_pdf: `{path}`");
        let html = self.to_html()?;
        html_to_pdf(&html, PathBuf::from(path).as_path())?;
        Ok(())
    }
}
