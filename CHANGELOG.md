## [1.0.17](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.16...v1.0.17) (2023-03-11)


### Bug Fixes

* **deps:** cargo update ([86d757b](https://gitlab.com/sbenv/veroxis/cv_generator/commit/86d757b712f3351127d2bbba0f89e71530080370))

## [1.0.16](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.15...v1.0.16) (2023-01-08)


### Bug Fixes

* update peer-dependencies ([f5ce9f9](https://gitlab.com/sbenv/veroxis/cv_generator/commit/f5ce9f90f01213fb4d629b6dda70ab50bf116b68))

## [1.0.15](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.14...v1.0.15) (2022-08-17)


### Bug Fixes

* **ron:** handle error type `SpannedError` ([764e4c5](https://gitlab.com/sbenv/veroxis/cv_generator/commit/764e4c547c8d9bf3e0420a831411824c16cdbf9a))

## [1.0.14](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.13...v1.0.14) (2022-08-11)


### Bug Fixes

* **clippy:** implement `Eq` ([361bd82](https://gitlab.com/sbenv/veroxis/cv_generator/commit/361bd823d302c11f7a64accd1d0cc559a9a3f5c7))

## [1.0.13](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.12...v1.0.13) (2022-07-04)


### Bug Fixes

* **clippy:** optimize string concatenation ([4b6ded7](https://gitlab.com/sbenv/veroxis/cv_generator/commit/4b6ded795478ca9194826dcdbd61f02aad0c2c39))

## [1.0.12](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.11...v1.0.12) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.5 ([ab9268a](https://gitlab.com/sbenv/veroxis/cv_generator/commit/ab9268a672359417192b4f99f7a2c6531c4bb42b))

## [1.0.11](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.10...v1.0.11) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate ron to 0.7.1 ([17f5422](https://gitlab.com/sbenv/veroxis/cv_generator/commit/17f5422e4eaad5120f1ec7991ea6e3909f169909))

## [1.0.10](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.9...v1.0.10) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.4 ([37e2ab8](https://gitlab.com/sbenv/veroxis/cv_generator/commit/37e2ab89325b8c13a2e7391c1981a353d4e708db))

## [1.0.9](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.8...v1.0.9) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.3 ([92b6647](https://gitlab.com/sbenv/veroxis/cv_generator/commit/92b6647351a465a45f3018bf5534b30e51fb832c))

## [1.0.8](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.7...v1.0.8) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.2 ([1094fc6](https://gitlab.com/sbenv/veroxis/cv_generator/commit/1094fc622b2037c9be8214ed72fd1b20896b838c))

## [1.0.7](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.6...v1.0.7) (2022-06-13)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.1 ([460ce27](https://gitlab.com/sbenv/veroxis/cv_generator/commit/460ce27988088951ba36b75dd7b84d6c8c89ecaf))
* use clap's new value_parser attribute ([65dcf54](https://gitlab.com/sbenv/veroxis/cv_generator/commit/65dcf5471d390eef64b117a4c6a9bd8cda469381))

## [1.0.6](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.5...v1.0.6) (2022-06-12)


### Bug Fixes

* **docs:** add descriptions within claps cli interface ([1e065c7](https://gitlab.com/sbenv/veroxis/cv_generator/commit/1e065c7b5df0c1c5b5aebb4e75d19600f281cc68))
* **style:** change arg name for consistency between `init` and `convert` ([f814811](https://gitlab.com/sbenv/veroxis/cv_generator/commit/f81481151bdd549f6694ee0ad47b212deae0f187))

## [1.0.5](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.4...v1.0.5) (2022-06-12)


### Bug Fixes

* **deps:** update rust crate ron to 0.7.0 ([98185eb](https://gitlab.com/sbenv/veroxis/cv_generator/commit/98185ebc5c6e4b6d7f6d7e7e650a87632eadfc02))

## [1.0.4](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.3...v1.0.4) (2022-06-12)


### Bug Fixes

* **deps:** update rust crate grass to 0.11.0 ([52bf282](https://gitlab.com/sbenv/veroxis/cv_generator/commit/52bf28265fcbc0406a2397405a9e2424630fd87a))

## [1.0.3](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.2...v1.0.3) (2022-06-12)


### Bug Fixes

* **deps:** update rust crate serde_yaml to 0.8.24 ([2fc2c54](https://gitlab.com/sbenv/veroxis/cv_generator/commit/2fc2c5476666ee5c6ef65dc11eae1b734e86e330))

## [1.0.2](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.1...v1.0.2) (2022-06-12)


### Bug Fixes

* **deps:** update rust crate serde_json to 1.0.81 ([1a46165](https://gitlab.com/sbenv/veroxis/cv_generator/commit/1a46165b4c875ba2573abbd0fcc6d7c6f04ffbfe))

## [1.0.1](https://gitlab.com/sbenv/veroxis/cv_generator/compare/v1.0.0...v1.0.1) (2022-06-12)


### Bug Fixes

* compress css in generated html ([d8deca5](https://gitlab.com/sbenv/veroxis/cv_generator/commit/d8deca561db5c3d12fbc7078ee0fec34cb8d9259))
* **deps:** update rust crate serde to 1.0.137 ([a239b7d](https://gitlab.com/sbenv/veroxis/cv_generator/commit/a239b7d37f845eb165ee68874d7c961cccc5967c))
* set workdir when file is converted to allow assets relative to config file ([92ff21e](https://gitlab.com/sbenv/veroxis/cv_generator/commit/92ff21ed316ee17c934d0a60cf897b1046652fee))

# 1.0.0 (2022-06-12)


### Features

* initial commit ([3e72a42](https://gitlab.com/sbenv/veroxis/cv_generator/commit/3e72a4236f2bb2232482d86f64cdb2a1d4b0430f))
