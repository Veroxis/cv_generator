module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: [
                'CHANGELOG.md',
                "Cargo.toml",
                "Cargo.lock",
                "cv_generator/Cargo.toml",
                "cv_generator/Cargo.lock",
                "cvgen/Cargo.toml",
                "cvgen/Cargo.lock",
            ],
        }],
        ['@semantic-release/gitlab', {
            assets: [
                {
                    label: `cvgen-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/cvgen`,
                },
            ],
        }]
    ],
};
